"use strict";

const buildServer = require("./buildServer");

const fastify = buildServer();

const start = async () => {
  try {
    await fastify.listen(process.env.PORT || 3000, "0.0.0.0");
    fastify.swagger();
    fastify.log.info(`server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    throw new Error(err);
  }
};
start();
