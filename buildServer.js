const _ = require("lodash");
const storage = require("./services/database/storage");
const config = require("./services/config");

function buildFastify() {
  const fastify = require("fastify")({
    logger: true
  });
  fastify.register(require("fastify-jwt"), {
    secret: config.authSecret
  });
  fastify.register(require("./plugins/mongoosePlugin")(config.mongoUrl));
  fastify.register(require("fastify-swagger"), {
    routePrefix: "/documentation",
    exposeRoute: true,
    swagger: {
      info: {
        title: "Meetups API",
        description: "API with meetups managment",
        version: "TBD"
      },
      externalDocs: {
        url: "https://gitlab.com/etienne_sanchez/meetups-api",
        description: "Find more info here"
      },
      host: "localhost:3000",
      schemes: ["http"],
      consumes: ["application/json"],
      produces: ["application/json"]
    }
  });
  fastify.register(require("./plugins/serverCachePlugin")());
  fastify.register(require("./features/authentication/routes")(storage));
  fastify.register(require("./features/users/routes")(storage));
  fastify.register(require("./features/meetups/routes")(storage));

  fastify.addHook("onRequest", async (request, reply) => {
    try {
      if (
        request.req.url !== "/signup" &&
        _.includes(["POST", "PUT", "DELETE"], request.req.method)
      ) {
        await request.jwtVerify();
      }
    } catch (err) {
      reply.send(err);
    }
  });

  return fastify;
}

module.exports = buildFastify;
