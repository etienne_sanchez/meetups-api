"use strict";
const {
  updateUserSchema,
  createUserSchema,
  getUsersSchema,
  deleteUserSchema,
  checkinSchema
} = require("./schemas");

module.exports = storage => {
  return async function routes(fastify) {
    const userStorage = storage(fastify.connectionObj, "User");
    fastify.post("/users", createUserSchema, async (request, reply) => {
      return userStorage
        .save(request.body)
        .then(data => reply.code(201).send(data));
    });
    fastify.put("/users/:userId", updateUserSchema, async (request, reply) => {
      return userStorage
        .update(request.body, request.params.userId)
        .then(data => reply.send(data));
    });
    fastify.get("/users", getUsersSchema, async (request, reply) => {
      return userStorage.list(request.query).then(data => reply.send(data));
    });
    fastify.delete(
      "/users/:userId",
      deleteUserSchema,
      async (request, reply) => {
        return userStorage
          .remove(request.params.userId)
          .then(data => reply.send(data));
      }
    );
    fastify.post(
      "/users/:userId/meetups/:meetupId",
      checkinSchema,
      async (request, reply) => {
        return userStorage
          .addReferencedObject(request.params.userId, {
            meetups: request.params.meetupId
          })
          .then(data => reply.send(data));
      }
    );
  };
};
