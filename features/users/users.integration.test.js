const request = require("supertest");
const { url, meetupId, userId } = require("../../services/config").integration;

jest.setTimeout(30000);

describe("in the users routes", () => {
  let userIdTemp, authToken;

  beforeAll(async () => {
    authToken = await getAuthorizationToken();
  });

  it("1. [POST] /users endpoint should create an user correctly", async () => {
    const response = await request(url)
      .post("/users")
      .send({
        name: "name",
        document: "document",
        email: "email",
        phoneNumber: 123456
      })
      .set("Authorization", authToken)
      .expect(201);
    userIdTemp = response.body.id;
  });

  it("2. [PUT] /users/{userId} endpoint should modify an user correctly", async () => {
    await request(url)
      .put(`/users/${userIdTemp}`)
      .send({ name: "name updated" })
      .set("Authorization", authToken)
      .expect(200);
  });

  it("3. [GET] /users endpoint should get a list of users correctly", async () => {
    await request(url)
      .get("/users")
      .expect(200);
  });

  it("4. [POST] /users/{userId}/meetups/{meetupId} endpoint should add a meetup to a guest(checkin)", async () => {
    await request(url)
      .post(`/users/${userIdTemp}/meetups/${meetupId}`)
      .set("Authorization", authToken)
      .expect(200);
  });

  it("5. [DELETE] /users/{userId} endpoint should delete an user", async () => {
    await request(url)
      .delete(`/users/${userIdTemp}`)
      .set("Authorization", authToken)
      .expect(200);
  });
});

const getAuthorizationToken = async () => {
  const response = await request(url)
    .post("/signup")
    .send({ userId });
  return `Bearer ${response.body.token}`;
};
