const headers = {
  type: "object",
  properties: {
    Authorization: { type: "string" }
  },
  required: ["Authorization"]
};

const updateUserSchema = {
  schema: {
    description: "endpoint to update an user",
    body: {
      type: "object",
      properties: {
        name: {
          type: "string"
        },
        document: {
          type: "string"
        },
        email: {
          type: "string"
        },
        phoneNumber: {
          type: "number"
        },
        meetups: { type: ["array", "null"] }
      }
    },
    params: {
      type: "object",
      properties: {
        userId: {
          type: "string"
        }
      }
    },
    headers
  }
};

const createUserSchema = {
  schema: {
    description: "endpoint to create a meetup",
    body: {
      ...updateUserSchema.schema.body,
      required: ["name", "document", "email"]
    },
    headers
  }
};

const getUsersSchema = {
  schema: {
    description: "endpoint to list the users",
    querystring: {
      name: {
        type: "string"
      },
      document: {
        type: "string"
      },
      email: {
        type: "string"
      },
      phoneNumber: {
        type: "number"
      }
    }
  }
};

const deleteUserSchema = {
  schema: {
    description: "endpoint to delete an user",
    params: {
      type: "object",
      properties: {
        userId: {
          type: "string"
        }
      }
    },
    headers
  }
};

const checkinSchema = {
  schema: {
    description: "endpoint to check-in an user in a meetup",
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        },
        guestId: {
          type: "string"
        }
      }
    },
    headers
  }
};

module.exports = {
  updateUserSchema,
  createUserSchema,
  getUsersSchema,
  deleteUserSchema,
  checkinSchema
};
