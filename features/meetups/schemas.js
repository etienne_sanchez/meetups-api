const headers = {
  type: "object",
  properties: {
    Authorization: { type: "string" }
  },
  required: ["Authorization"]
};

const updateMeetupSchema = {
  schema: {
    description: "endpoint to update a meetup",
    body: {
      type: "object",
      properties: {
        title: {
          type: "string"
        },
        description: {
          type: "string"
        },
        startAt: {
          type: "string"
        },
        guests: { type: ["array", "null"] }
      }
    },
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        }
      }
    },
    headers
  }
};

const createMeetupSchema = {
  schema: {
    description: "endpoint to create a meetup",
    body: {
      ...updateMeetupSchema.schema.body,
      required: ["title", "description", "startAt"]
    },
    headers
  }
};

const getMeetupsSchema = {
  schema: {
    description: "endpoint to list the meetups",
    querystring: {
      title: {
        type: "string"
      },
      description: {
        type: "string"
      },
      startAt: {
        type: "string"
      }
    }
  }
};

const deleteMeetupSchema = {
  schema: {
    description: "endpoint to delete a meetup",
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        }
      }
    },
    headers
  }
};

const addGuestToMeetupSchema = {
  schema: {
    description: "endpoint to add a guest to a meetup",
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        },
        guestId: {
          type: "string"
        }
      }
    },
    headers
  }
};

const removeGuestFromMeetupSchema = {
  schema: {
    description: "endpoint to remove a guest from a meetup",
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        },
        guestId: {
          type: "string"
        }
      }
    },
    headers
  }
};

const getMeetupTemperatureSchema = {
  schema: {
    description: "endpoint to get the temperature in the meetup day",
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        }
      }
    }
  }
};

const getBeerboxesByMeetupSchema = {
  schema: {
    description: "endpoint to get the necessaries beer boxes to cover a meetup",
    params: {
      type: "object",
      properties: {
        meetupId: {
          type: "string"
        }
      }
    }
  }
};

module.exports = {
  updateMeetupSchema,
  createMeetupSchema,
  getMeetupsSchema,
  deleteMeetupSchema,
  addGuestToMeetupSchema,
  removeGuestFromMeetupSchema,
  getMeetupTemperatureSchema,
  getBeerboxesByMeetupSchema
};
