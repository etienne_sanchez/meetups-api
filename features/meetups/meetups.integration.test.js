const request = require("supertest");
const { url, userId } = require("../../services/config").integration;

jest.setTimeout(30000);

describe("in the meetups routes", () => {
  let meetupId, authToken;

  beforeAll(async () => {
    authToken = await getAuthorizationToken();
  });

  it("1. [POST] /meetups endpoint should create a meetup correctly", async () => {
    const response = await request(url)
      .post("/meetups")
      .send({
        title: "title",
        description: "description",
        startAt: new Date().toISOString()
      })
      .set("Authorization", authToken)
      .expect(201);
    meetupId = response.body.id;
  });

  it("2. [PUT] /meetups/{meetupId} endpoint should modify a meetup correctly", async () => {
    await request(url)
      .put(`/meetups/${meetupId}`)
      .send({ description: "description updated" })
      .set("Authorization", authToken)
      .expect(200);
  });

  it("3. [GET] /meetups endpoint should get a list of meetups correctly", async () => {
    await request(url)
      .get("/meetups")
      .expect(200);
  });

  it("4. [POST] /meetups/{meetupId}/guests/{guestId} endpoint should add a guest to a meetup", async () => {
    await request(url)
      .post(`/meetups/${meetupId}/guests/${userId}`)
      .set("Authorization", authToken)
      .expect(200);
  });

  it("5. [DELETE] /meetups/{meetupId}/guests/{guestId} endpoint should delete a guest from a meetup", async () => {
    await request(url)
      .delete(`/meetups/${meetupId}/guests/${userId}`)
      .set("Authorization", authToken)
      .expect(200);
  });

  it("6. [GET] /meetups/{meetupId}/beerBoxes endpoint should get the beer boxes of a meetup", async () => {
    await request(url)
      .get(`/meetups/${meetupId}/beerBoxes`)
      .expect(200);
  });

  it("7. [GET] /meetups/{meetupId}/temperature endpoint should get the temperature of a meetup", async () => {
    await request(url)
      .get(`/meetups/${meetupId}/temperature`)
      .expect(200);
  });

  it("8. [DELETE] /meetups/{meetupId} endpoint should delete a meetup", async () => {
    await request(url)
      .delete(`/meetups/${meetupId}`)
      .set("Authorization", authToken)
      .expect(200);
  });
});

const getAuthorizationToken = async () => {
  const response = await request(url)
    .post("/signup")
    .send({ userId });
  return `Bearer ${response.body.token}`;
};
