"use strict";

const {
  updateMeetupSchema,
  createMeetupSchema,
  getMeetupsSchema,
  deleteMeetupSchema,
  addGuestToMeetupSchema,
  removeGuestFromMeetupSchema,
  getMeetupTemperatureSchema,
  getBeerboxesByMeetupSchema
} = require("./schemas");

module.exports = storage => {
  return async function routes(fastify) {
    const meetupStorage = storage(fastify.connectionObj, "Meetup");
    const state = {
      storage: meetupStorage,
      cache: fastify.serverCache
    };
    const meetupService = require("../../services/meetupService")(state);
    fastify.post("/meetups", createMeetupSchema, async (request, reply) => {
      return meetupStorage
        .save(request.body)
        .then(data => reply.code(201).send(data));
    });
    fastify.put(
      "/meetups/:meetupId",
      updateMeetupSchema,
      async (request, reply) => {
        return meetupStorage
          .update(request.body, request.params.meetupId)
          .then(data => reply.send(data));
      }
    );
    fastify.get("/meetups", getMeetupsSchema, async (request, reply) => {
      return meetupStorage.list(request.query).then(data => reply.send(data));
    });
    fastify.delete(
      "/meetups/:meetupId",
      deleteMeetupSchema,
      async (request, reply) => {
        return meetupStorage.remove(request.params.meetupId).then(data => {
          if (!data) {
            return reply
              .code(410)
              .send({ message: "Resource already deleted" });
          }
          return reply.send(data);
        });
      }
    );
    fastify.post(
      "/meetups/:meetupId/guests/:guestId",
      addGuestToMeetupSchema,
      async (request, reply) => {
        return meetupService
          .addGuestToMeetup(request.params.meetupId, request.params.guestId)
          .then(data => reply.send(data));
      }
    );
    fastify.delete(
      "/meetups/:meetupId/guests/:guestId",
      removeGuestFromMeetupSchema,
      async (request, reply) => {
        return meetupStorage
          .removeReferencedObject(request.params.meetupId, {
            guests: request.params.guestId
          })
          .then(data => reply.send(data));
      }
    );
    fastify.get(
      "/meetups/:meetupId/temperature",
      getMeetupTemperatureSchema,
      async (request, reply) => {
        return meetupService
          .getMeetupTemperature(request.params.meetupId)
          .then(data => reply.send(data));
      }
    );
    fastify.get(
      "/meetups/:meetupId/beerBoxes",
      getBeerboxesByMeetupSchema,
      async (request, reply) => {
        return meetupService
          .getBeerBoxesByMeetup(request.params.meetupId)
          .then(data => reply.send(data));
      }
    );
  };
};
