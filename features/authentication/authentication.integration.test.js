const request = require("supertest");
const { url, userId } = require("../../services/config").integration;

jest.setTimeout(30000);

describe("in the authentication routes", () => {
  it("[POST] /signup endpoint should return the correct token", async () => {
    await request(url)
      .post("/signup")
      .send({
        userId
      })
      .expect(200);
  });
});
