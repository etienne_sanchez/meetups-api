"use strict";
const schema = require("./schema");

module.exports = storage => {
  return async function routes(fastify) {
    const userStorage = storage(fastify.connectionObj, "User");
    fastify.post("/signup", schema, (req, reply) => {
      return userStorage.list({ _id: req.body.userId }).then(users => {
        const user = users[0];
        if (user) {
          const token = fastify.jwt.sign(req.body);
          return reply.send({ token });
        }
        return reply.code(400).send({ message: "Invalid payload" });
      });
    });
  };
};
