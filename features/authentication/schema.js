const body = {
  type: "object",
  required: ["userId"],
  properties: {
    userId: {
      type: "string"
    }
  }
};

module.exports = {
  schema: { body, description: "Endpoint to get the authorization token" }
};
