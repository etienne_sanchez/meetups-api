'use strict';

const mongoose = require('mongoose');
const fp = require('fastify-plugin');
const logger = require('pino')({ name: 'plugin - mongoose' });

module.exports = (connectionString, name = 'connectionObj') => {
    const close = name => async fastify => {
        try {
            await fastify[name].close();
        } catch (err) {
            return err;
        }
    };

    function mongooseConnection(fastify, opt, next) {
        logger.info('connecting to {url}', { url: connectionString });
        mongoose
            .createConnection(connectionString, { useNewUrlParser: true })
            .then(connection => {
                logger.info('connected to {url}', { url: connectionString });
                fastify.decorate(name, connection);
                fastify.addHook('onClose', close(name));
                next();
            })
            .catch(err => {
                logger.error('error connecting to {url} with {error}', {
                    url: connectionString,
                    error: err
                });
                next();
            });

    }
    return fp(mongooseConnection);
};
