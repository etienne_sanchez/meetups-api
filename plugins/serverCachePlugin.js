'use strict';

const fp = require('fastify-plugin');
const { Cache } = require('memory-cache');

module.exports = (name = 'serverCache') => {
    function serverCache(fastify, opt, next) {
        fastify.decorate(name, new Cache());
        next();
    }
    return fp(serverCache);
};
