# Meetups API

This API is thought to manage the meetups invitations, creation, beer provision, etc.

## Prerequisites

- `node`
- `npm`
- `Postman` or any other tool to make requests

## Authentication

Each request that modifies the API data, must be executed passing it a Bearer token obtained from the `/signup` endpoint.

## Basic Workflow

Supposing that the API URL base is `API_URL_BASE` and at least one user is created in the database, then:

1. Create a meetup
    ```
    [POST] API_URL_BASE/meetups
    ```
2. Get the list of users
    ```
    [GET] API_URL_BASE/users
    ```
3. Invite to a user adding it to the meetup and an email will be sent to the user
    ```
    [POST] API_URL_BASE/meetups/:meetupId/guest/:userId
    ```
4. Check-in a user, adding the meetup to the user
    ```
    [POST] API_URL_BASE/users/:userId/meetups/:meetupId
    ```
    That's all, for more information about the endpoints, visit the swagger documentation
    ```
    [GET] API_URL_BASE/documentation
    ```

## Tests API locally

1. Clone the git repository
    ```
    git clone https://gitlab.com/etienne_sanchez/meetups-api.git 
    ```
2. Navigate to the repository root
    ```
    cd meetups-api
    ```
3. Install the dependencies
    ```
    npm i
    ```
4. Add a `.env` file to the project root
    ```
    MONGODB_URI=<mongo url extracted from Heroku>
    SENDGRID_API_KEY=<Sengrid Api Key extracted from Heroku>
    EMAIL_SENDER=<email sender extracted from Heroku>
    AUTH_SECRET=<Auth extracted from Heroku>
    DARK_SKY_TOKEN=<DarkSky token extracted from Heroku>
    ```
    __Note:__ To obtain the environment variables values, please ask me for them. (etidj1995@gmail.com).
5. Run the `start` script
    ```
    npm run start
    ```
    In the console, will be appear the ext log

    ```
    {"level":30,"time":1582147913211,"pid":26324,"hostname":"<COMPUTER_NAME>","msg":"Server     listening at http://0.0.0.0:3000","v":1}
    ```
    So, the `http://0.0.0.0:3000` will the `API_URL_BASE`.
5. Having the API url base, run the [basic workflow](#basic-workflow).


## CI/CD

The API has a pipeline that runs the unit tests on each change, and deploy to Heroku on each change in the `master` branch. The stages are:

1. unit tests
2. deploy

Once the API is deployed on Heroku, the [basic workflow](#basic-workflow) can be tested perfectly.

## Some debts:
- Coverage reporting
- More unit tests