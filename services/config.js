require("dotenv").config();

module.exports = {
  mongoUrl: process.env.MONGODB_URI,
  sengridApiKey: process.env.SENDGRID_API_KEY,
  emailSender: process.env.EMAIL_SENDER,
  authSecret: process.env.AUTH_SECRET,
  cabaGeoposition: { latitude: -34.6131516, longitude: -58.3772316 },
  darkSkyToken: process.env.DARK_SKY_TOKEN,
  integration: {
    meetupId: process.env.INTEGRATION_MEETUP_ID,
    userId: process.env.INTEGRATION_USER_ID,
    url: process.env.INTEGRATION_URL
  }
};
