const sengridAPI = require("./sengridAPI");

describe("sengridAPI component should", () => {
    const request = {
        method: "POST",
        path: "/v3/mail/send",
        body: {}
    };
    const guest = { name: "guest fake" };
    const meetup = { startAt: "2020-02-19T04:37:33.940Z", title: "meetup fake" };
    const mockAPI = {
        emptyRequest: jest.fn().mockReturnValue(request),
        API: jest.fn().mockResolvedValue({})
    };
    jest.mock("sendgrid", () => () => mockAPI);
    it("call to Sengrid API with the correct parameters", async () => {
        await sengridAPI.sendEmail(guest, meetup);
        expect(mockAPI.API).toHaveBeenCalledWith(request);
    });
    it("return the correct value", async () => {
        const result = await sengridAPI.sendEmail(guest, meetup);
        expect(result).toStrictEqual({
            message:
                "Great. guest fake has been invited successfully to the meetup: meetup fake!"
        });
    });
    it("throw an error", async () => {
        const expectedError = new Error();
        mockAPI.API = jest.fn().mockRejectedValue(expectedError);
        expect(sengridAPI.sendEmail(guest, meetup)).rejects.toThrow(expectedError);
    });
});
