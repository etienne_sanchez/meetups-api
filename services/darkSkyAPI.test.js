const darkSkyAPI = require("./darkSkyAPI");
const axios = require("axios");
const config = require('./config');

describe("darkSkyAPI component should", () => {
    const darkSkyToken = "darkSkyToken";
    jest.mock("axios");
    process.env.darkSkyToken = darkSkyToken;
    const timestamp = Date.UTC();
    it("call to axios get with the correct parameters", async () => {
        axios.get = jest.fn().mockResolvedValue({});
        const { cabaGeoposition, darkSkyToken } = config,
            { latitude, longitude } = cabaGeoposition;
        await darkSkyAPI.getWeather(timestamp);
        expect(axios.get).toHaveBeenCalledWith(
            `https://api.darksky.net/forecast/${darkSkyToken}/${latitude},${longitude},${timestamp}?units=si`
        );
    });
    it("return the correct value", async () => {
        const expectedResult = { data: 'data' }
        axios.get = jest.fn().mockResolvedValue(expectedResult);
        const result = await darkSkyAPI.getWeather(timestamp);
        expect(result).toStrictEqual(expectedResult.data);
    });
    it("throw an error", async () => {
        const expectedError = new Error();
        axios.get = jest.fn().mockRejectedValue(expectedError);
        expect(darkSkyAPI.getWeather(timestamp)).rejects.toThrow(expectedError);
    });
});
