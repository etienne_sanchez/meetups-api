const save = Document => data => {
    const date = new Date();
    const document = new Document({
        ...data,
        _insertedAt: date
    });
    return fnExecutor(() => document.save());
};

const update = Document => (data, id) => {
    const date = new Date();
    data._updatedAt = date;
    return fnExecutor(() =>
        Document.findByIdAndUpdate(id, { $set: data }, { new: true })
    );
};

const list = Document => query => {
    return fnExecutor(() => Document.find(query || {}));
};

const remove = Document => id => {
    return fnExecutor(() => Document.findByIdAndRemove(id));
};

const addReferencedObject = Document => (parentId, childToPush, populateOptions) => {
    return fnExecutor(() => {
        const updated = Document.findByIdAndUpdate(parentId, { $addToSet: childToPush }, { new: true });
        if (populateOptions) {
            const { childName, parentName } = populateOptions;
            return updated.populate(childName, `-__v -${parentName}`)
        }
        return updated;
    }
    );
};

const removeReferencedObject = Document => (parentId, childToPush) => {
    return fnExecutor(() =>
        Document.findByIdAndUpdate(parentId, { $pull: childToPush }, { new: true })
    );
};

const fnExecutor = fn =>
    fn()
        .then(data => data)
        .catch(error => {
            throw error;
        });

module.exports = (conn, model) => {
    const Document = conn[model]
        ? conn[model]
        : conn.model(model, require(`./models/${model}`));
    return {
        save: save(Document),
        update: update(Document),
        remove: remove(Document),
        list: list(Document),
        addReferencedObject: addReferencedObject(Document),
        removeReferencedObject: removeReferencedObject(Document)
    };
};
