'use strict';
const Schema = require('mongoose').Schema;

const User = new Schema({
    name: String,
    document: String,
    email: String,
    phoneNumber: Number,
    meetups: [{
        type: Schema.Types.ObjectId,
        ref: 'Meetup'
    }]
});

User.virtual('id').get(function () {
    return this._id.toHexString();
});

User.set('toJSON', {
    virtuals: true,
    transform(doc, ret) {
        delete ret['_id'];
    }
});
module.exports = User;