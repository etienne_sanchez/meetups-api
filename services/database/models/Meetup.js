'use strict';
const Schema = require('mongoose').Schema;

const Meetup = new Schema({
    title: String,
    description: String,
    startAt: Date,
    _insertedAt: Date,
    _updatedAt: Date,
    guests: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]
});

Meetup.virtual('id').get(function () {
    return this._id.toHexString();
});

Meetup.set('toJSON', {
    virtuals: true,
    transform(doc, ret) {
        delete ret['_id'];
    }
});
module.exports = Meetup;