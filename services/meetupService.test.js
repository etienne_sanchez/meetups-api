const meetupService = require("./meetupService");
const emailAPI = require("./sengridAPI");
const weatherAPI = require("./darkSkyAPI");
const { Cache } = require("memory-cache");
const _ = require("lodash");

describe("in the meetupService, the", () => {
  const date = "2020-02-19T04:37:33.940Z";
  const parsedDate = Date.parse(date) / 1000;
  const cache = new Cache();
  cache.put(parsedDate, 50);
  const meetup = {
    startAt: date,
    id: "meetup1",
    guests: [{ id: "guest1" }, { id: "guest2" }]
  };
  weatherAPI.getWeather = jest.fn();
  const mockStorage = {
    list: jest.fn().mockResolvedValue([meetup]),
    addReferencedObject: jest.fn().mockResolvedValue(meetup)
  };
  const state = { storage: mockStorage, cache };

  const commonTests = methodName => {
    it("call to the storage list method with the correct parameters", async () => {
      await meetupService(state)[methodName](meetup.id);
      expect(mockStorage.list).toHaveBeenCalledWith({ _id: meetup.id });
    });
    it("obtain the temperature from the cache without call to weather API", async () => {
      weatherAPI.getWeather.mockClear();
      await meetupService(state)[methodName](meetup.id);
      expect(weatherAPI.getWeather).not.toHaveBeenCalled();
    });
    it("obtain the temperature from the weather API and put the result in the cache", async () => {
      cache.clear();
      const weatherAPIResponse = {
        currently: { temperature: 10, apparentTemperature: 13 }
      };
      weatherAPI.getWeather = jest.fn().mockResolvedValue(weatherAPIResponse);
      await meetupService(state)[methodName](meetup.id);
      expect(weatherAPI.getWeather).toHaveBeenCalledWith(parsedDate);
      expect(cache.get(parsedDate)).toBe(
        _.mean([
          weatherAPIResponse.currently.temperature,
          weatherAPIResponse.currently.apparentTemperature
        ])
      );
    });
  };

  describe("addGuestToMeetup method should", () => {
    it("call to the storage with the correct parameters and return the correct value", async () => {
      const expectedResult = { message: "everything OK!" };
      emailAPI.sendEmail = jest.fn().mockResolvedValue(expectedResult);
      const result = await meetupService(state).addGuestToMeetup(
        meetup.id,
        meetup.guests[0].id
      );
      expect(mockStorage.addReferencedObject).toHaveBeenCalledWith(
        meetup.id,
        {
          guests: meetup.guests[0].id
        },
        { childName: "guests", parentName: "meetups" }
      );
      expect(emailAPI.sendEmail).toHaveBeenCalledWith(meetup.guests[0], meetup);
      expect(result).toStrictEqual(expectedResult);
    });
  });

  describe("getMeetupTemperature method should", () => {
    commonTests("getMeetupTemperature");

    it("return the correct value", async () => {
      const result = await meetupService(state).getMeetupTemperature(meetup.id);
      expect(result).toStrictEqual({ temperature: cache.get(parsedDate) });
    });
  });

  describe("getBeerBoxesByMeetup method should", () => {
    commonTests("getBeerBoxesByMeetup");

    it("return 2 beer boxes when temperature is between 20 inclusive and 24 inclusive", async () => {
      [20, 23.15, 24].forEach(async temperature => {
        cache.put(parsedDate, temperature);
        await expect(
          meetupService(state).getBeerBoxesByMeetup(meetup.id)
        ).resolves.toStrictEqual({ beersBoxesNecessaries: 2 });
      });
    });

    it("return 1.5 beer boxes when temperature is lower than 20 exclusive", async () => {
      cache.put(parsedDate, 19.5);
      await expect(
        meetupService(state).getBeerBoxesByMeetup(meetup.id)
      ).resolves.toStrictEqual({ beersBoxesNecessaries: 1.5 });
    });

    it("return 4 beer boxes when temperature is more than 24 exclusive", async () => {
      cache.put(parsedDate, 24.3);
      await expect(
        meetupService(state).getBeerBoxesByMeetup(meetup.id)
      ).resolves.toStrictEqual({ beersBoxesNecessaries: 4 });
    });
  });
});
