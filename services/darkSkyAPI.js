const axios = require('axios');
const config = require('./config');

const getWeather = timestamp => {
    const { cabaGeoposition, darkSkyToken } = config,
        { latitude, longitude } = cabaGeoposition;
    return axios.get(`https://api.darksky.net/forecast/${darkSkyToken}/${latitude},${longitude},${timestamp}?units=si`)
        .then(function (response) {
            return response.data;
        })
        .catch(function (error) {
            throw error;
        });
}

module.exports = { getWeather }