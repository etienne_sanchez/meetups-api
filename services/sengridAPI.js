const { Email, Content, Mail } = require("sendgrid").mail;
const fs = require("fs");
const config = require("./config");

const readEmailFile = path => {
  try {
    const filename = require.resolve(path);
    return fs.readFileSync(filename, "utf8");
  } catch (e) {
    throw e;
  }
};

const replaceValues = fileAsString => meetup => {
  return fileAsString
    .replace(/@meetupTitle/g, meetup.title)
    .replace(/@meetupStartDate/g, meetup.startAt.toString())
    .replace(/@meetupDescription/g, meetup.description);
};

module.exports = {
  sendEmail: (guest, meetup) => {
    const from_email = new Email(config.emailSender);
    const to_email = new Email(guest.email);
    const subject = `Invitation to ${meetup.title}`;
    const content = new Content(
      "text/html",
      replaceValues(readEmailFile("../statics/emailTemplate.html"))(meetup)
    );
    const mail = new Mail(from_email, subject, to_email, content);

    const sg = require("sendgrid")(config.sengridApiKey);
    const request = sg.emptyRequest({
      method: "POST",
      path: "/v3/mail/send",
      body: mail.toJSON()
    });

    return sg
      .API(request)
      .then(_ => {
        return {
          message: `Great. ${guest.name} has been invited successfully to the meetup: ${meetup.title}!`
        };
      })
      .catch(error => {
        throw error;
      });
  }
};
