const emailAPI = require("./sengridAPI");
const weatherAPI = require("./darkSkyAPI");
const _ = require("lodash");

const addGuestToMeetup = storage => (meetupId, guestId) => {
    return storage
        .addReferencedObject(
            meetupId,
            {
                guests: guestId
            },
            { childName: "guests", parentName: "meetups" }
        )
        .then(meetup =>
            emailAPI.sendEmail(
                _.find(meetup.guests, guest => guest.id === guestId),
                meetup
            )
        );
};

const getMeetupTemperature = ({ storage, cache }) => meetupId => {
    return storage
        .list({ _id: meetupId })
        .then(meetups => getTemperature(cache)(meetups[0]))
        .then(temperature => ({ temperature }));
};

const getBeerBoxesByMeetup = ({ storage, cache }) => meetupId => {
    return storage.list({ _id: meetupId }).then(meetups =>
        getTemperature(cache)(meetups[0]).then(temperature => {
            const beersByPerson = betBeersByPerson(temperature);
            return {
                beersBoxesNecessaries: meetups[0].guests.length * beersByPerson
            };
        })
    );
};

const toTimestamp = strDate => {
    var datum = Date.parse(strDate);
    return datum / 1000;
};

const betBeersByPerson = temperature => {
    let beersByPerson;
    if (_.inRange(temperature, 20, 24) || _.isEqual(temperature, 24)) {
        beersByPerson = 1;
    } else if (_.lt(temperature, 20)) {
        beersByPerson = 0.75;
    } else {
        beersByPerson = 2;
    }
    return beersByPerson;
};

const getTemperature = cache => meetup => {
    const timestamp = toTimestamp(meetup.startAt);
    let cacheValue = cache.get(timestamp);
    if (!cacheValue) {
        return weatherAPI.getWeather(timestamp).then(data => {
            const { temperature, apparentTemperature } = data.currently;
            const meanTemperature = _.mean([temperature, apparentTemperature]);
            cache.put(timestamp, meanTemperature);
            return meanTemperature;
        });
    }
    return Promise.resolve(cacheValue);
};

module.exports = state => {
    return {
        addGuestToMeetup: addGuestToMeetup(state.storage),
        getMeetupTemperature: getMeetupTemperature(state),
        getBeerBoxesByMeetup: getBeerBoxesByMeetup(state)
    };
};
